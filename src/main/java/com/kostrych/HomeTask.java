package com.kostrych;

import sun.reflect.generics.scope.ConstructorScope;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class HomeTask {
    @MyAnnotation(name= "MyAnnotation", number = 1)
    public static void main(String[] args){
        SomeClass someClass = new SomeClass();
        Class clazz = someClass.getClass();
        Constructor[] constructors = clazz.getConstructors();
        Method[] methods = clazz.getMethods();
        for (Constructor constructor: constructors) {
            System.out.println(constructor.getName());
        }

        for (Method method: methods) {
            System.out.println(method.getDeclaredAnnotations());
        }
    }
}
