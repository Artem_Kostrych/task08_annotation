package com.kostrych;

public class SomeClass {
    private String name= "1";

    public SomeClass() {
    }

    public SomeClass(String name) {
        this.name = name;
    }

    public SomeClass(String name, int i) {
        this.name = name;
    }

    @MyAnnotation(name = "s", number = 12)
    @Override
    public String toString() {
        return "SomeClass{" +
                "name='" + name + '\'' +
                '}';
    }
}
