package com.kostrych;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.CLASS)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@interface MyAnnotation {
    String name ();
    boolean run () default true;
    int number();
    String value() default "none";

}
